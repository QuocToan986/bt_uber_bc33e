const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
//  giá tiền của từng loại xe
function giaTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  } else if (car == UBER_SUV) {
    return 9000;
  } else {
    return 10000;
  }
}
function giaTienKm1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
  }
}
function giaTienKm19TroDi(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
  }
}

// hàm onclick
function tinhTienUber() {
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;
  console.log("carOption: ", carOption);

  // giá tiền của từng loại xe
  var giaTienKmDauTienValue = giaTienKmDauTien(carOption);
  console.log("giaTienKmDauTienValue: ", giaTienKmDauTienValue);

  var giaTienKm1_19Value = giaTienKm1_19(carOption);
  console.log("giaTienKm1_19Value: ", giaTienKm1_19Value);

  var giaTienKm19TroDiValue = giaTienKm19TroDi(carOption);
  console.log("giaTienKm19TroDiValue: ", giaTienKm19TroDiValue);

  // lấy sokm từ input
  var soKm = document.getElementById("txt-km").value * 1;
  // output
  var tienTra = 0;
  //tính tiền xe
  if (soKm > 0 && soKm <= 1) {
    tienTra = soKm * giaTienKmDauTienValue;
  } else if (soKm > 1 && soKm <= 19) {
    tienTra = giaTienKmDauTienValue + (soKm - 1) * giaTienKm1_19Value;
  } else {
    tienTra =
      giaTienKmDauTienValue +
      (18 * giaTienKm1_19 + (soKm - 19) * giaTienKm19TroDiValue);
  }
  document.getElementById("xuatTien").innerHTML =
    tienTra.toLocaleString() + " " + "VND";
}
